#!/bin/bash

# Variables
SOURCES="sources"
LANG="fr_FR.UTF-8"
LANGS="en de"
LOCALES="locales"
POT=${LOCALES}/pot

# Recherche les fichiers adoc du dossier sources
FILES=$(find $SOURCES  -maxdepth 1 -name *.adoc)
# Pour chaque fichier trouvé :
# Génère les fichiers pot et po s'ils n'existent pas
# Met à jour les fichiers pot et po s'ils existent
for file in $FILES
do
  echo "Traitement du fichier $file"
  # Extraire le nom du fichier puis le nom sans extension
  filename=$(basename "$file")
  name="${filename%.*}";
  potfile=${POT}/${name}.pot
  if test -f $potfile
  then
    echo "Mise à jour du fichier $potfile"
    # mise à jour du fichier pot
    po4a-updatepo -f asciidoc -m $file -p ./$potfile
  else
    echo "Creation du fichier $potfile"
    # creation du fichier pot
    po4a-gettextize -f asciidoc -M utf-8 -m $file -p ./$potfile
  fi
  for lang in $LANGS
  do
    pofile=$LOCALES/$lang/${name}.po
    if test -f $pofile
    then
      echo "Mise à jour du fichier $pofile"
      # mise à jour du fichier po
      po4a-updatepo -f asciidoc -m $file -p $pofile
    else
      echo "Création du fichier $pofile"
      # creation du fichier po
      msginit --locale=$lang --input=$potfile --output=$pofile  
    fi
  done
done
