#!/bin/bash

# depends : Asciidoctor-pdf
# Installation
# gem install --pre asciidoctor-pdf
#
# Parametres
# $1 = chemin vers le chemin de build
#
# Variables :
DIRNAME=$(dirname $0)
SOURCES="$DIRNAME/../sources/"
WEBSITE="$DIRNAME/../siteweb/"
DEST="$DIRNAME/../$1/"
DATE=$(date +%x) # La date au format Local
LANGS="en de"
LOCALES="$DIRNAME/../locales"

# Test si un argument est fourni
if [ $# -eq 0 ]
  then
    echo "Aucun argument fourni : public ou build"
    exit 1
fi

# Traduction des documents
# Recherche les fichiers adoc du dossier sources
FILES=$(find $SOURCES -maxdepth 1 -name *.adoc)
for file in $FILES
do
  for lang in $LANGS
  do
    # Extraire le nom du fichier puis le nom sans extension
    filename=$(basename "$file")
    name="${filename%.*}";
    echo "Traduction du fichier ${name}_${lang}.adoc"
    po4a-translate -f asciidoc -M utf-8 -m $file -p ${LOCALES}/${lang}/${name}.po -k 0 -l ${SOURCES}/${lang}/${name}_${lang}.adoc 
  done
done

# Generation des fichiers PDF
FILES=$(find $SOURCES -name *.adoc)
for file in $FILES
do
  echo "Debut de la compilation du fichier ${file}"
  asciidoctor-pdf                            \
    -a pdf-stylesdir="${SOURCES}/theme/"     \
    -a pdf-style="asciidoctor"               \
    -a pdf-fontsdir="${SOURCES}/theme/fonts" \
    -t                                       \
    -n                                       \
    -a revdate="${DATE}"                     \
    -a lang="fr"                             \
    -a icons="font"                          \
    -a encoding="utf-8"                      \
    -a toc="preamble"                        \
    -a toc-title="Table des matières"        \
    -a toclevels=3                           \
    -D $DEST ${file}
  echo "Fin de la compilation du fichier ${file}"
  echo "----------------------------------------"
  echo ""
done

if test "$1" = "public"
then
  FILES=$(find $WEBSITE -name *.adoc)
  for file in $FILES
  do
    echo "Debut de la compilation du fichier ${file}"
    asciidoctor -D $DEST ${file}
    echo "Fin de la compilation du fichier ${file}"
    echo "----------------------------------------"
    echo ""
  done
fi

